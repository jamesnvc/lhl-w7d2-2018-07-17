//
//  ViewController.swift
//  MoreAnimationsDemo
//
//  Created by James Cash on 17-07-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var pinkViewHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        let view1 = UIView(frame: CGRect(x: 10, y: 10, width: 250, height: 250))
        view1.backgroundColor = UIColor.red
        view.addSubview(view1)

        // most basic animation
        /*
        UIView.animate(withDuration: 2) {
            view1.backgroundColor = UIColor.blue
        }
         */

        print("Calling animate")
        UIView.animate(withDuration: 2, delay: 0,
                       options: [.repeat, .autoreverse, .curveEaseIn],
                       animations: {
                        view1.backgroundColor = UIColor.blue
                        view1.frame.origin = CGPoint(x: 150, y: 350)
        }) { (finished) in
            print("Finished animation")
        }
        print("finished calling animate")

        // spring animations
        view1.isHidden = true

        let view2 = UIView(frame: CGRect(x: 150, y:0 , width: 150, height: 150))
        view2.backgroundColor = UIColor.purple
        view.addSubview(view2)

        UIView.animate(withDuration: 2, delay: 0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 0,
                       options: [.repeat],
                       animations: {
                        view2.center = CGPoint(x: 225, y: 300)
        }, completion: nil)

        // keyframes
        view2.isHidden = true

        let view3 = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 250))
        view3.backgroundColor = UIColor.cyan
        view.addSubview(view3)

        UIView.animateKeyframes(
            withDuration: 3, delay: 0,
            options: [.repeat],
            animations: {
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.3, animations: {
                    view3.backgroundColor = UIColor.blue
                    view3.frame.origin = CGPoint(x: 250, y: 0)
                })
                UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.25, animations: {
                    view3.backgroundColor = UIColor.red
                    view3.frame.origin = CGPoint(x: 250, y: 300)
                })
                UIView.addKeyframe(withRelativeStartTime: 0.55, relativeDuration: 0.45, animations: {
                    view3.backgroundColor = UIColor.purple
                    view3.frame.origin = CGPoint(x: 0, y: 700)
                })
        }, completion: nil)

        // transitions
        view3.isHidden = true

        view4 = UIView(frame: CGRect(x: 150, y: 150, width: 200, height: 200))
        view4!.backgroundColor = UIColor.red
        view.addSubview(view4!)

        view5 = UIView(frame: CGRect(x: 0, y: 200, width: 300, height: 100))
        view5?.backgroundColor = UIColor.green

        let btn = UIButton(type: .roundedRect)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Switch views", for: .normal)
        view.addSubview(btn)
        btn.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        btn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        btn.addTarget(self, action: #selector(switchViews(sender:)), for: UIControlEvents.touchUpInside)


        let pinkView = self.pinkViewHeightConstraint.firstItem as! UIView
        let pinkTopConstraint = pinkView.topAnchor.constraint(equalTo: self.view.topAnchor)
        // animating autolayout
        UIView.animate(withDuration: 2) {
            // method 1 of animating with layout constraints:
            // just change the constant of one or more constraints
//            self.pinkViewHeightConstraint.constant = 300

            // method 2:
            // deactive some constraints, activate other ones
            self.pinkViewHeightConstraint.isActive = false
            pinkTopConstraint.isActive = true

            // eitherway, always need to do this when animating autolayout constraints
            self.view.layoutIfNeeded()
        }
    }

    @objc func switchViews(sender: UIButton) {
        if view4?.superview != nil {
            UIView.transition(with: view,
                              duration: 2,
                              options: [.transitionFlipFromBottom, .allowAnimatedContent],
                              animations: {
                                self.view4!.removeFromSuperview()
                                self.view.addSubview(self.view5!)
                                sender.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            }, completion: nil)
        } else {
            UIView.transition(with: view,
                              duration: 2,
                              options: [.transitionCrossDissolve, .allowAnimatedContent],
                              animations: {
                                self.view5!.removeFromSuperview()
                                self.view.addSubview(self.view4!)
                                sender.transform = CGAffineTransform(rotationAngle: 0)
            }, completion: nil)
        }

    }

    var view4: UIView?
    var view5: UIView?

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

